import java.util.regex.Pattern

import sbt._
import Keys._
import org.scalatra.sbt.ScalatraPlugin
import com.mojolly.scalate.ScalatePlugin._
import ScalateKeys._
import com.earldouglas.xwp.XwpPlugin._
import sbt.Project.Initialize
import sbt.classpath.ClasspathUtilities

import MyDistPlugin.{DistKeys => keys}

object build extends Build {
  val Organization = "com.frobinet"
  val Name = "UCL Social Network"
  val Version = "0.2.0-SNAPSHOT"
  val ScalaVersion = "2.11.7"
  val ScalatraVersion = "2.4.0"


  lazy val project = Project (
    "ucl-social-network",
    file("."),
    settings = ScalatraPlugin.scalatraSettings ++ scalateSettings ++
      MyDistPlugin.distSettings ++ Seq(
      mainClass in MyDistPlugin.Dist := Some("com.frobinet.uclsocialnetwork.ScalatraLauncher"),
      keys.envExports in MyDistPlugin.Dist := Seq("LC_CTYPE=en_US.UTF-8", "LC_ALL=en_US.utf-8"),
      javaOptions in MyDistPlugin.Dist ++= Seq("-Xss4m", "-Dfile.encoding=UTF-8", "-Dorg.scalatra.environment=production"),
      keys.runScriptName in MyDistPlugin.Dist := "run_server"
    ) ++ Seq(
      organization := Organization,
      name := Name,
      version := Version,
      scalaVersion := ScalaVersion,
      libraryDependencies ++= Seq(
        "org.scalatra" %% "scalatra" % ScalatraVersion,
        "org.scalatra" %% "scalatra-scalate" % ScalatraVersion,
        "org.scalatra" %% "scalatra-specs2" % ScalatraVersion % "test",
        "com.typesafe.slick" %% "slick" % "3.0.0",
        "org.postgresql" % "postgresql" % "9.4.1211",
        "com.typesafe.play" %% "play-json" % "2.3.4",
        "com.mchange" % "c3p0" % "0.9.5.2",
        "ch.qos.logback" % "logback-classic" % "1.1.7",
        "ch.qos.logback" % "logback-core" % "1.1.7",
        "javax.servlet" % "javax.servlet-api" % "3.1.0" % "provided",
        "org.eclipse.jetty" % "jetty-webapp" % "9.2.14.v20151106",
        "com.typesafe.play" %% "play-ws" % "2.4.3"
      ),
      resolvers += "Typesafe Repo" at "http://repo.typesafe.com/typesafe/releases/",
      scalateTemplateConfig in Compile <<= (sourceDirectory in Compile){ base =>
        Seq(
          TemplateConfig(
            base / "webapp" / "WEB-INF" / "templates",
            Seq.empty,  /* default imports should be added here */
            Seq.empty,  /* add extra bindings here */
            Some("templates")
          )
        )
      }
    )
  ).settings(addArtifact(artifact in (Compile, keys.dist), keys.dist): _*).
    settings(addArtifact(Artifact("projectName", "zip", "zip"), keys.dist): _*)
}





object MyDistPlugin extends Plugin {

  import DistKeys._
  object DistKeys {
    val dist = taskKey[File]("Build a distribution, assemble the files, create a launcher and make an archive.")
    val stage = taskKey[Seq[File]]("Build a distribution, assemble the files and create a launcher.")
    val assembleJarsAndClasses = taskKey[Seq[File]]("Assemble jars and classes")
    val envExports = settingKey[Seq[String]]("The exports which will be expored in the launcher script.")
    val runScriptName = settingKey[String]("The name of the service runscript.")
  }

  val Dist = config("dist")

  private def assembleJarsAndClassesTask: Initialize[Task[Seq[File]]] =
    (fullClasspath in Runtime, excludeFilter in Dist, target in Dist) map { (cp, excl, tgt) =>
      IO.delete(tgt)
      val (libs, dirs) = cp.map(_.data).toSeq partition ClasspathUtilities.isArchive
      val jars = libs.descendantsExcept(GlobFilter("*"), excl) pair flat(tgt / "lib")
      val classesAndResources = dirs flatMap { dir =>
        val files = dir.descendantsExcept(GlobFilter("*"), excl)
        files pair rebase(dir, tgt / "lib")
      }

      (IO.copy(jars) ++ IO.copy(classesAndResources)).toSeq
    }

  private def createLauncherScriptTask(base: File, name: String, libFiles: Seq[File], mainClass: Option[String], javaOptions: Seq[String], envExports: Seq[String], logger: Logger): File = {
    val f = base / "bin" / name
    if (!f.getParentFile.exists()) f.getParentFile.mkdirs()
    IO.write(f, createScriptString(base, name, libFiles, mainClass, javaOptions, envExports))
    "chmod +x %s".format(f.getAbsolutePath) ! logger
    f
  }

  private def createScriptString(base: File, name: String, libFiles: Seq[File], mainClass: Option[String], javaOptions: Seq[String], envExports: Seq[String]): String = {
    """
      |#!/bin/env bash
      |
      |export CLASSPATH="lib:%s"
      |export JAVA_OPTS="%s"
      |%s
      |
      |java $JAVA_OPTS -cp $CLASSPATH %s
      |
    """.stripMargin.format(classPathString(base, libFiles), javaOptions.mkString(" "), envExports.map(e => "export %s".format(e)).mkString("\n"), mainClass.getOrElse(""))
  }

  private def classPathString(base: File, libFiles: Seq[File]) = {
    (libFiles filter ClasspathUtilities.isArchive map (_.relativeTo(base))).flatten mkString java.io.File.pathSeparator
  }

  private def stageTask: Initialize[Task[Seq[File]]] =
    (webappDest in Compile, baseDirectory in Compile, excludeFilter in Dist, assembleJarsAndClasses in Dist, target in Dist, runScriptName in Dist, mainClass in Dist, javaOptions in Dist, envExports in Dist, streams) map { (webRes, baseDir, excl, libFiles, tgt, nm, mainClass, javaOptions, envExports, s) =>
      val launch = createLauncherScriptTask(tgt, nm, libFiles, mainClass, javaOptions, envExports, s.log)
      val logsDir = tgt / "logs"
      if (!logsDir.exists()) logsDir.mkdirs()

      val publicDir = baseDir / "public"
      s.log.info(s"Copying $publicDir to dist in $tgt/public")
      val publicFilesFinder = publicDir.descendantsExcept(GlobFilter("*"), excl)
      val publicFiles = IO.copy(publicFilesFinder pair rebase(publicDir, tgt / "public"))

      val secretDir = baseDir / "secret"
      s.log.info(s"Copying $secretDir to dist in $tgt/secret")
      val secretFilesFinder = secretDir.descendantsExcept(GlobFilter("*"), excl)
      val secretFiles = IO.copy(secretFilesFinder pair rebase(secretDir, tgt / "secret"))

      // Some of the files in "secret" need to be copied to lib folder to be taken into account (e.g. custom c3p0.properties)
      // Caution: important to add these files after the original resources files
      val secretConfigFiles = IO.copy(secretFilesFinder pair rebase(secretDir, tgt / "lib"), overwrite = true)

      s.log.info("Adding " + webRes + " to dist in " + tgt + "/webapp")
      val resourceFilesFinder = webRes.descendantsExcept(GlobFilter("*"), excl)
      val resourceFiles = IO.copy(resourceFilesFinder pair rebase(webRes, tgt / "webapp"))

      libFiles ++ Seq(launch, logsDir) ++ resourceFiles ++ publicFiles ++ secretFiles ++ secretConfigFiles
    }

  private def distTask: Initialize[Task[File]] =
    (stage in Dist, target in Dist, name in Dist, version in Dist) map { (files, tgt, nm, ver) =>
      val zipFile = tgt / ".." / (nm + "-" + ver + ".zip")
      val paths = files pair rebase(tgt, nm)
      IO.zip(paths, zipFile)
      zipFile
    }

  private class PatternFileFilter(val pattern: Pattern) extends FileFilter {
    def accept(file: File): Boolean = {
      pattern.matcher(file.getCanonicalPath).matches
    }
  }

  private object PatternFileFilter {
    def apply(expression: String): PatternFileFilter = new PatternFileFilter(Pattern.compile(expression))
  }

  val distSettings = {
    Seq(
      excludeFilter in Dist := {
        HiddenFileFilter || PatternFileFilter(".*/WEB-INF/classes") || PatternFileFilter(".*/WEB-INF/lib")
        // could use (webappDest in webapp).value.getCanonicalPath instead of .*, but webappDest is a task and SBT settings cant depend on tasks
      },
      target in Dist <<= (target in Compile)(_ / "dist"),
      assembleJarsAndClasses in Dist <<= assembleJarsAndClassesTask,
      stage in Dist <<= stageTask,
      dist in Dist <<= distTask,
      dist <<= dist in Dist,
      name in Dist <<= name,
      runScriptName in Dist <<= name,
      mainClass in Dist := Some("ScalatraLauncher"),
      envExports in Dist := Seq()
    )
  }

}

