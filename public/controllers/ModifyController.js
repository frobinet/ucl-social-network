var myApp = angular.module("UCLSocialNetwork", [ 'ngCookies', 'ngMaterial' ])

myApp.controller("ModifyController", ['$scope', '$cookies', '$http', function ($scope, $cookies, $http) {

	$scope.showFailure = false
	$scope.showSuccess = false

    accessToken = null

	$scope.facebook = {
	    connected: false,
	    picture: "",
	    name: "",
	    id: "",
	    token: ""
	}

	window.fbAsyncInit = function() {
        FB.init({
            appId      : '1677805592501086',
            xfbml      : false, // No Social Plugin on the page so not needed
            version    : 'v2.8'
        });

        FB.getLoginStatus(function(response) {
          if (response.status === 'connected') {
            accessToken = response.authResponse.accessToken
            onLogin()
          }
        });
    };

    (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "http://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));


	$scope.facebookLogin = function() {
        FB.login(onLogin, { scope: ["user_friends", "public_profile"] });
    }

    function onLogin() {
        FB.api("/me?fields=id,name,picture", "get", { type: "large", redirect: false }, function(response) {
            $http({
                method	: 'GET',
                'url' 	: '/friend/' + response["id"],
                "headers": { "Content-type": "application/json" },
            })
            .then(function success(existence) {
                $scope.facebook.connected = true
                $scope.facebook.name = response["name"]
                $scope.facebook.picture = response["picture"]["data"]["url"]
                $scope.facebook.id = response["id"]
                $scope.facebook.alreadyRegistered = existence["data"]["exists"]
                console.log(response)
            }, function failure(exception) {
                $scope.showFailure = true
                $scope.showSuccess = false
                $scope.failureText = exception.data
                console.log(exception)
            })
        })
    }

    function setCookies(id, token) {
        $cookies.put("facebook-id", id)
        $cookies.put("facebook-token", token)
    }


    $scope.disconnect = function() {
        $cookies.remove("facebook-id")
        $cookies.remove("facebook-token")
        $scope.facebook = {
            connected: false
        }
    }


	$scope.addFriend = function() {

		setCookies($scope.facebook.id, accessToken)

        $http({
            method	: 'POST',
            'url' 	: '/auth/friend',
            "headers": { "Content-type": "application/json" },
            "withCredentials": true
        })
        .then(function success(response) {
            $scope.showSuccess = true
            $scope.showFailure = false
            $scope.facebook.alreadyRegistered = true
        }, function failure(response) {
            $scope.showFailure = true
            $scope.showSuccess = false
            $scope.failureText = response.data
            console.log(response)
        })
	}


	$scope.removeFriend = function() {
        setCookies($scope.facebook.id, accessToken)

        $http({
            method	: 'DELETE',
            'url' 	: '/auth/friend',
            "headers": { "Content-type": "application/json" },
            "withCredentials": true
        })
        .then(function success(response) {
            $scope.showSuccess = true
            $scope.showFailure = false
            $scope.facebook.alreadyRegistered = true

        }, function failure(response) {
            $scope.showFailure = true
            $scope.showSuccess = false
            $scope.facebook.alreadyRegistered = false
            $scope.failureText = response.data
            console.log(response)
        })
	}
}])

