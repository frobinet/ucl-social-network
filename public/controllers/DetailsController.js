var myApp = angular.module("UCLSocialNetwork")

myApp.controller("DetailsController", ['$scope', '$controller', '$mdDialog', function ($scope, $controller, $mdDialog) {

	function DetailsDialogController($scope, $mdDialog, dataToPass) {
		
		$scope.hide = function() { $mdDialog.hide() }
		$scope.cancel = function() { $mdDialog.cancel() }

		$scope.selectedIndex = dataToPass.selectedIndex
		$scope.selected = dataToPass.friends[dataToPass.selectedIndex]
		
		var community = dataToPass.communities[$scope.selected[dataToPass.options.community]]
		
		// Rankings computation for each kind of centrality
		$scope.rankings = []
		for(var i = 0 ; i < dataToPass.options.centrality_options.length ; i++) {
			
			var name = dataToPass.options.centrality_options[i].value // Centrality name
			var ranking = {
				text: dataToPass.options.centrality_options[i].text // Centrality display name
			}
			ranking["value"] = $scope.selected[name] // Centrality value

			// Compute rankings in community
			var r_community = 1
			for(var j = 0 ; j < community.length ; j++) {
				if(ranking["value"] < community[j][name])
					r_community += 1
			}

			// Compute overall ranking
			var r_overall = 1
			for(var j = 0 ; j < dataToPass.friends.length ; j++) {
				if(ranking["value"] < dataToPass.friends[j][name])
					r_overall += 1
			}
			
			ranking["community_rank"] = r_community
			ranking["overall_rank"] = r_overall
			$scope.rankings.push(ranking) // Add this ranking to rankings
			$scope.community_size = community.length
			$scope.friends_size = dataToPass.friends.length
		}
	}

	// Show Details dialog
	$scope.showDetailDialog = function(ev) {
		$mdDialog.show({
			locals: { dataToPass: $scope }, // Pass this scope to DetailsDialogController
			controller: DetailsDialogController,
			templateUrl: '/templates/DetailsDialog.html',
			parent: angular.element(document.body),
			targetEvent: ev,
			clickOutsideToClose: true
		})
	}
}])

