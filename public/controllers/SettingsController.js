var myApp = angular.module("UCLSocialNetwork")

myApp.controller("SettingsController", ['$scope', '$controller', '$mdDialog', function ($scope, $controller, $mdDialog) {

	function OptionsDialogController($scope, $mdDialog, dataToPass) {
		$scope.hide = function() { $mdDialog.hide() }
		$scope.cancel = function() { $mdDialog.cancel() }
		
		$scope.answer = function(answer) { 
			var redraw = false
			if(answer != "cancel") {
				if(dataToPass.set.centrality != dataToPass.centrality)
					dataToPass.centrality = dataToPass.set.centrality
				if(dataToPass.set.show != dataToPass.show)
					dataToPass.show = dataToPass.set.show
			}

			// This will redraw the graph if redraw is true
			// For now redraw is always false, but this is useful if other settings,
			// e.g. new community detection algorithms need support
			$mdDialog.hide(redraw)
		}

		$scope.options = dataToPass
	}

	$scope.showOptionsDialog = function(ev) {
		$scope.options.set.show = $scope.options.show
		$scope.options.set.centrality = $scope.options.centrality
		$mdDialog.show({
			locals: { dataToPass: $scope.options },
			controller: OptionsDialogController,
			templateUrl: '/templates/SettingsDialog.html',
			parent: angular.element(document.body),
			targetEvent: ev,
			clickOutsideToClose: true
		})
		.then(function(redraw) {
			// Redraw the graph if community changed
			if(redraw)
				$scope.refreshDisplay()
		});
	}
}])

