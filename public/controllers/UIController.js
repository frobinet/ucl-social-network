var myApp = angular.module("UCLSocialNetwork", [ 'ngMaterial' ])
var controller = myApp.controller("UIController", function ($scope, $http, $location) {

		/*
		 * SETTINGS
		 */
		$scope.options = {
			// Default values
			"show": "all",
			"centrality": "eigenvector", // Default Centrality
			"community": "community", // Default Community

			set: {}, // Used by SettingsController

			// Possible values
			"show_options": [
				{"value": "all", "text": "Show All"},
				{"value": "only_friends", "text": "Show Friends"}
			],
			"centrality_options": [
				{ "value": "degree", "text": "Degree" },
				{ "value": "eigenvector", "text": "Eigenvector" },
				{ "value": "closeness", "text": "Closeness" },
				{ "value": "betweenness", "text": "Betweenness" },
			], 
			"community_options": [
				{ "value": "community", "text": "Louvain" }
			]
		}

		/* 
		 * Searching and filtering 
		 */
		// Used to filter only friends
		$scope.keep_relevant = function(value, index, array) {
			if($scope.selectedIndex == index)
				return false

			if(!checkSearch(index, $scope.search_centralities))
				return false
			
			if($scope.options.show == "all")
				return true
			
			if($scope.options.show == "only_friends") {
				var selected = $scope.friends[$scope.selectedIndex]
				return selected.friends[index]
			}
		}

		// Used by keep_relevant to 
		function checkSearch(index, search) {
			if(search) {
				if(!$scope.friends[index])
					return false
				search = search.toLowerCase()
				var name = $scope.friends[index].name.toLowerCase()
				if(name.indexOf(search) < 0) {
					return false
				}
			}
			return true
		}

		/*
		 * PEOPLE SELECTION
		 */
		// To allow moving a node to the front when selecting it
		d3.selection.prototype.moveToFront = function() {
			return this.each(function(){
				this.parentNode.appendChild(this);
			});
		};

		// Used by UI on click to select someone in the graph
		$scope.selectPerson = function(index, id) {
			var node = d3.select($scope.nodeMap[id])
			selectNode(node, index, apply=false)
		}

		// Marks a node as selected, changing its size and stroke
		// Changing $scope.selectedIndex automatically refreshes the UI
		function selectNode(node, index, apply) {
			node.moveToFront()
			if(apply == null) apply = true
			var radius = $scope.radius
			if($scope.selectedNode) {
				$scope.selectedNode.attr("r", radius)
				$scope.selectedNode.attr("class", "node")
			}
			node.attr("r", 1.4*radius)
			node.attr("class", "node selected")
			$scope.selectedNode = node
			$scope.selectedIndex = index
			if(apply == true)
				$scope.$apply()
		}

		/* 
		 * COMPUTATIONS 
		 */
		// Computes a dictionary of friends for each person
		function computeFriends(graph) {
			var links = graph.edges,
				nodes = graph.nodes

			for(var i=0 ; i < links.length ;  i++) {
				var link = links[i]
				var source = nodes[link.source],
					target = nodes[link.target]
				if(!source.friends)
					source.friends = {}
				source.friends[link.target] = true

				if(!target.friends)
					target.friends = {}
				target.friends[link.source] = true
			}	
		}

		// Computes a dictionary of the different communities
		function computeCommunities(graph) {
			var nodes = graph.nodes
			var communities = {}
			var numberOfCommunities = 0
			for(var i = 0 ; i < nodes.length ; i++) {
				var c = nodes[i][$scope.options.community]
				if(!communities[c]) {
					numberOfCommunities += 1
					communities[c] = []
				}
				communities[c].push(nodes[i])
			}
			return { "communities": communities, "numberOfCommunities": numberOfCommunities }
		}


		function drawGraph($scope) {
			var graph = $scope.graph

			// Find container size
			var container_size = d3.select("#graph_container").node().getBoundingClientRect()
			var width = container_size.width,
				height = container_size.height,
				color = d3.scale.category20();

			$scope.radius = 5

			var svg = d3.select("#graph_container").append("svg")
				.attr("width", width)
				.attr("height", height)
				.attr("id", "graph_svg")
				.append("g")
				.call(d3.behavior.zoom().scaleExtent([-5, 8]).on("zoom", zoom))
				.append("g")
			$scope.svg = true

			function zoom() {
				svg.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
			}

			svg.append("rect")
				.attr("class", "overlay")
				.attr("width", width)
				.attr("height", height)

			var force = d3.layout.forceInABox()
				.charge(-90)
				.linkDistance(20)
				.linkStrengthInterCluster(0.008)
				.gravityToFoci(2.0)
				.gravityOverall(0.1)
				.size([width, height])
				.nodes(graph.nodes)
				.links(graph.edges)
				.groupBy(function (d) { return d[$scope.options.community] })

			var links = svg.selectAll(".link")
				.data(graph.edges)
				.enter().append("line")
				.attr("class", "link")

			$scope.nodeMap = {}
			var nodes = svg.selectAll(".node")
				.data(graph.nodes)
				.enter()
				.append("circle")
				.attr("class", "node")
				.attr("id", function(d) { return d.id; })
				.attr("r", $scope.radius)
				.style("fill", function(d) {
					$scope.nodeMap[d.id] = this;
					return color(d[$scope.options.community]); 
				})

			// Initial position for nodes: position nodes next to their community peers
			nodes.each(function(d) {
				var i = d[$scope.options.community]
				var m = $scope.numberOfCommunities
				d.x = Math.cos(i / m * 2 * Math.PI) * 0.3 * width + width / 2 + 50*Math.random()
				d.y = Math.sin(i / m * 2 * Math.PI) * 0.3 * height + height / 2 + 50*Math.random()
			})

			force.start()
			$scope.force = force // To be able to stop the force
			$scope.forceRunning = true

			firstNode = d3.select(".node")
			firstIndex = firstNode.data()[0].index
			selectNode(firstNode, firstIndex, false)

			var frozen = false
			nodes.on("click", function(d) {
				if(force.alpha() > 0) return
				if(!frozen) {
					nodes.each(function(d) { d.fixed = true })
					frozen = true
				}
				node = d3.select(this)
				selectNode(node, node.data()[0].index)
			})

			nodes.append("title").text(function(d) { return d.name; });

			force.on("tick", function() {
				links
					.attr("x1", function(d) { return d.source.x; })
					.attr("y1", function(d) { return d.source.y; })
					.attr("x2", function(d) { return d.target.x; })
					.attr("y2", function(d) { return d.target.y; });

				nodes
					.attr("cx", function(d) { return d.x; })
					.attr("cy", function(d) { return d.y; });
			});

		}

	/*
	 * GRAPH DISPLAY AND ANIMATIONS
	 */
	// Used to stop/start the graph animation
	$scope.toggleForce = function() {
		$scope.forceRunning = !$scope.forceRunning
		if($scope.forceRunning)
			$scope.force.resume()
		else
			$scope.force.stop()
	}

	function refreshDisplay() {
		var o = computeCommunities($scope.graph)
		$scope.communities = o.communities
		$scope.numberOfCommunities = o.numberOfCommunities
		if($scope.svg) {
			d3.select("#graph_svg").remove()
			$scope.svg = false
		}
		drawGraph($scope)
	}
	$scope.refreshDisplay = refreshDisplay



	/*
	 * RUN WHEN PAGE LOADS TO LOAD DATA AND DRAW GRAPH
	 */ 
	if(!$scope.svg) {
		// Load data and draw graph
		var graphPath = '/network'
		$http.get(graphPath).success(function(graph) {
            // Map id to index in graph.nodes' array
		    var idToIndex = {}
            for(var i = 0 ; i < graph.nodes.length ; i++) {
                var node = graph.nodes[i]
                idToIndex[node.id] = i
            }

            // Transform edges to use indexes instead of ids
            for(var i = 0 ; i < graph.edges.length ; i++) {
                var edge = graph.edges[i]
                edge.source = idToIndex[edge.source]
                edge.target = idToIndex[edge.target]
            }

            // Make graph available in scope and compute friends
			$scope.graph = graph
			computeFriends(graph)
			$scope.friends = graph.nodes

			refreshDisplay()
		})
	}

})
