package com.frobinet.uclsocialnetwork

import java.sql.BatchUpdateException
import javax.servlet.ServletContext

import grizzled.slf4j.Logging
import org.scalatra._
import play.api.libs.json._
import slick.driver.PostgresDriver.api._
import slick.jdbc.meta.MTable

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import scala.language.postfixOps
import FriendsAccessor._
import com.frobinet.uclsocialnetwork.FacebookAPI.fields.{Id, Name}

import scala.util.{Failure, Success}


trait Routes extends ScalatraBase with FutureSupport { self: App =>

  val adminIds = Set("10206526036539127")
  def db: Database

  def token: String = cookies.get("facebook-token").getOrElse(throw new IllegalArgumentException(s"No id found in cookies: $cookies"))
  def id: String = cookies.get("facebook-id").getOrElse(throw new IllegalArgumentException(s"No token found in cookies: $cookies"))

  def serveStaticFile(path: String, extension: String) = {
    // Content type (server will try to infer others)
    if(extension == "html") contentType = "text/html"
    else if(extension == "js") contentType = "application/javascript"
    else if(extension.endsWith("css")) contentType = "text/css"

    val filename = s"$path.$extension"
    val file = new java.io.File("public/" + filename)
    if(file.exists()) {
      Ok(file)
    } else {
      debug(s"File not found in public folder: $filename")
      NotFound()
    }
  }

  get("/network") {
    contentType = "application/json"
    Json.stringify(SocialNetwork.toJSON)
  }

  def verifyIdAndTokenMatch(): Unit = {
    // Check that id and token match, or that the token belongs to an admin
    val tokenOpt = cookies.get("facebook-token")
    val idOpt = cookies.get("facebook-id")
    if(tokenOpt.isEmpty || idOpt.isEmpty) halt(BadRequest("Couldn't not find id or token inside cookie"))
    else {
      val result = Await.result(FacebookAPI.checkTokenValidity(token, id, adminIds), 20 seconds)
      if(result.status.code != 200) halt(result)
      else pass() // Token is fine: Allow processing to continue
    }
  }

  get("/") {
    serveStaticFile("index", "html")
  }

  get("/modify") {
    serveStaticFile("modify", "html")
  }

  before("/auth/*") {
    verifyIdAndTokenMatch()
  }

  before("/admin/*") {
    if(cookies.get("facebook-id").isEmpty || !adminIds.contains(id)) Forbidden("This action is reserved to administrators")
    verifyIdAndTokenMatch()
  }

  post("/auth/friend") {
    // Fetch information from the user on Facebook
    val fieldsFuture = FacebookAPI.fetchFields(token, Set(Name, Id)).flatMap { fields =>
      val name = fields(Name.value) match {
        case JsDefined(JsString(value)) => value
      }
      val id = fields(Id.value) match {
        case JsDefined(JsString(value)) => value
      }
      Future.successful(name, id)
    }

    // Fetch picture
    val urlFuture = FacebookAPI.fetchPicture(token)

    // Fetch user's friends
    val friendsFuture = FacebookAPI.fetchFriends(token)

    // TODO Subscription or just batch update every once in a while

    val totalFuture = (fieldsFuture zip urlFuture) zip friendsFuture
    val finalFuture = totalFuture flatMap { case (((name, id), picture), friends) =>
      debug(s"Got results: name = $name, id = $id, picture = $picture, friends = $friends")
      val knownFriends = friends.filter(SocialNetwork.containsFriend)
      val friendships = knownFriends.map(friendId => Friendship(id, friendId))
      self.friendsAccessor.createFriend(id, name, picture, friendships)
    }

    Await.ready(finalFuture, 40 seconds)
    finalFuture.value.get match {
      case Success(_) => Ok()
      case Failure(t) =>
        error(s"Couldn't Create user: $t")
        t.printStackTrace()
        InternalServerError("Coudn't Create User")
    }

  }

  // Batch update to refresh the profiles and friends of every registered users, using the registered tokens
  post("/admin/update") {
    // TODO
    InternalServerError("Not Implemented yet")
  }

  get("/friend/:id") {
    val id = params("id")
    val response = JsObject(Seq("exists" -> JsBoolean(SocialNetwork.containsFriend(id))))
    Ok(response)
  }

  delete("/auth/friend") {
    self.friendsAccessor.deleteFriend(id).onComplete {
      case Success(_) => Ok()
      case Failure(t) =>
        error(s"Couldn't delete user: $t")
        t.printStackTrace()
        InternalServerError(s"Could not delete user: ${t.getMessage}")
    }
  }
}

class App(val context: ServletContext, val db: Database) extends ScalatraServlet with FutureSupport with Routes with Logging {

  protected implicit def executor = scala.concurrent.ExecutionContext.Implicits.global
  protected val env = context.getInitParameter(org.scalatra.EnvironmentKey)

  val francois = Friend("876540", "François", "")
  val charlotte = Friend("297861", "Charlotte", "")
  val alexis = Friend("0772", "Alexis", "")
  val maxime = Friend("3244", "Maxime", "")
  val laura = Friend("434", "Laura", "")
  val marine = Friend("345", "Marine", "")

  val friendshipsName = Friendships.baseTableRow.tableName
  val friendsName = Friends.baseTableRow.tableName

  private def execIfNotInTables(tables: Vector[MTable], tableName: String)(creation: => Future[Unit]): Future[Unit] = {
    if (!tables.exists(_.name.name == tableName)) {
      creation
    } else {
      Future.successful(())
    }
  }

  val createIfNotExists = (db: Database) => db.run(MTable.getTables).flatMap(tables =>
    execIfNotInTables(tables, friendsName)(db.run(Friends.schema.create)).flatMap { Unit =>
      execIfNotInTables(tables, friendshipsName)(db.run(Friendships.schema.create))
    }
  )

  val drop = DBIO.seq(
    sqlu"""DROP TABLE IF EXISTS "#$friendshipsName"""",
    sqlu"""DROP TABLE IF EXISTS "#$friendsName""""
  )

  val populate = DBIO.seq(
    // Populate DB
    Friends ++= Seq(francois, charlotte, alexis, maxime, laura, marine),
    Friendships ++= Seq(
      francois <-> charlotte, francois <-> alexis, francois <-> maxime, francois <-> laura,
      charlotte <-> alexis, charlotte <-> laura, charlotte <-> marine,
      alexis <-> maxime
    )
  )

  val dbCreation = createIfNotExists(db).andThen {
    case Success(()) =>
      if (env == "development") db.run(populate.transactionally)
    case Failure(throwable) =>
      throwable.printStackTrace()
      db.run(drop) // Drop any created table
  }

  val friendsAccessor = new FriendsAccessor(db)
  val networkData = dbCreation.flatMap { Unit =>
    db.run(friendsAccessor.getAllFriendsAndFriendships)
  }

  try {
    val (friends, friendships) = Await.result(networkData, 20 seconds)
    SocialNetwork.addFriends(friends)
    SocialNetwork.addFriendships(friendships)
    debug(s"SocialNetwork Loaded: ${SocialNetwork.graph.getNodeCount} nodes, ${SocialNetwork.graph.getEdgeCount} edges")
    SocialNetwork.updateMetrics()
  } catch {
    case e: BatchUpdateException => e.getNextException.printStackTrace()
  }


}
