package com.frobinet.uclsocialnetwork

import java.sql.Timestamp

import play.api.libs.json._


class Friendship(
  id1: String,
  id2: String,
  val creationTimestamp: Timestamp = new Timestamp(System.currentTimeMillis())
) {
  require(id1 != id2, s"Can't be friend with yourself!")
  val (aId, bId) = if(id1 < id2) (id1, id2) else (id2, id1) // Always store ordered pairs

  def toJSON = JsObject(Seq(
    "source" -> JsString(aId),
    "target" -> JsString(bId),
    "creation_timestamp" -> JsNumber(creationTimestamp.getTime)
  ))

  override def toString: String = s"$aId <--> $bId"
}

object Friendship {
  def apply(id1: String, id2: String, timestamp: Timestamp = new Timestamp(System.currentTimeMillis())): Friendship =
    new Friendship(id1, id2, timestamp)
  def unapply(f: Friendship): Option[(String, String, Timestamp)] = Some((f.aId, f.bId, f.creationTimestamp))
}

