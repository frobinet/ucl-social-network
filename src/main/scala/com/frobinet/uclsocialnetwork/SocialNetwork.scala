package com.frobinet.uclsocialnetwork

import grizzled.slf4j.Logging
import org.gephi.graph.api._
import org.gephi.project.api.ProjectController
import org.gephi.statistics.plugin.{Degree, EigenvectorCentrality, GraphDistance, Modularity}
import org.openide.util.Lookup
import play.api.libs.json._

import scala.collection.JavaConverters._

object SocialNetwork extends Logging {

  val pc = Lookup.getDefault.lookup[ProjectController](classOf[ProjectController])
  pc.newProject()
  val workspace = pc.getCurrentWorkspace

  //Get a graph model - it exists because we have a workspace
  //val graphModel = Lookup.getDefault.lookup[GraphController](classOf[GraphController]).getGraphModel(workspace)
  val configuration = new Configuration()
  configuration.setNodeIdType(classOf[String]) // Store ids as Int instead of default Strings
  val graphModel = GraphModel.Factory.newInstance(configuration)

  // value column to store the concrete Friend
  val nameColumn =  graphModel.getNodeTable.addColumn("name", classOf[String])
  val pictureColumn = graphModel.getNodeTable.addColumn("picture", classOf[String])
  val eigenvectorCentralityColumn = graphModel.getNodeTable.addColumn(EigenvectorCentrality.EIGENVECTOR, "Eigenvector Centrality", classOf[java.lang.Double], Double.box(0d))
  val closenessCentralityColumn = graphModel.getNodeTable.addColumn(GraphDistance.CLOSENESS, "Closeness Centrality", classOf[java.lang.Double], Double.box(0d))
  val betweennessCentralityColumn = graphModel.getNodeTable.addColumn(GraphDistance.BETWEENNESS, "Betweenness Centrality", classOf[java.lang.Double], Double.box(0d))
  val degreeColumn = graphModel.getNodeTable.addColumn(Degree.DEGREE, "Degree", classOf[java.lang.Integer], Int.box(0))
  val modularityClassColumn = graphModel.getNodeTable.addColumn(Modularity.MODULARITY_CLASS, "Modularity", classOf[java.lang.Integer], Int.box(0))

  // Undirected Graph
  val graph = graphModel.getUndirectedGraph

  def containsFriend(id: String): Boolean = graph.hasNode(id)

  def allFriends: Set[Friend] = synchronized {
    graph.getNodes.asScala.map(_.toFriend).toSet
  }

  def allFrienships: Set[Friendship] = synchronized {
    graph.getEdges.asScala.map(_.toFriendship).toSet
  }

  def addFriends(friends: Set[Friend]): Unit = synchronized {
    val newNodes = friends.map(friendToNode)
    graph.addAllNodes(newNodes.asJava)
    updateMetrics()
  }

  def addFriendships(friendships: Set[Friendship]): Unit = synchronized {
    val edges = friendships.map { case Friendship(aId, bId, _) =>
      val a = graph.getNode(aId)
      val b = graph.getNode(bId)
      graphModel.factory().newEdge(a, b, false)
    }
    edges.foreach(graph.addEdge)
    updateMetrics()
  }

  def getFriends(id: String): Set[Friend] = {
    val node = graph.getNode(id)
    val neighbors = graph.getNeighbors(node).toCollection.asScala
    neighbors.map(_.toFriend).toSet
  }

  def getFriends(friend: Friend): Set[Friend] = getFriends(friend.fbId)

  def removeFriend(id: String): Unit = synchronized {
    val node = graph.getNode(id)
    graph.clearEdges(node)
    graph.removeNode(node)
    updateMetrics()
  }

  def removeFriend(friend: Friend): Unit = removeFriend(friend.fbId)

  def removeFriendship(aId: String, bId: String): Unit = synchronized {
    val a = graph.getNode(aId)
    val b = graph.getNode(bId)
    graph.removeEdge(graph.getEdge(a, b))
  }

  def removeFriendship(friendship: Friendship): Unit = removeFriendship(friendship.aId, friendship.bId)

  def updateMetrics() = synchronized {
    val start = System.currentTimeMillis
    val distance = new GraphDistance()
    distance.setDirected(false)
    distance.execute(graphModel)

    val degreeTask = new Degree()
    degreeTask.execute(graphModel)

    val eigenvectorTask = new EigenvectorCentrality()
    eigenvectorTask.execute(graphModel)

    val communityTask = new Modularity()
    communityTask.setRandom(false) // To get deterministic results
    communityTask.setUseWeight(false) // Unweighted links
    //communityTask.setResolution() // Could be used later on to pick # communities
    communityTask.execute(graphModel)

    // Note: Metrics are not persisted to DB for now, and are recomputed on server startup
    debug(s"Recomputed Metrics for ${graph.getNodeCount} nodes and ${graph.getEdgeCount} edges in ${System.currentTimeMillis-start} ms")
  }

  private implicit class FriendNode(val node: Node) {
    def toFriend: Friend = {
      val id = node.getId.asInstanceOf[String]
      val name = node.getAttribute(nameColumn).asInstanceOf[String]
      val picture = node.getAttribute(pictureColumn).asInstanceOf[String]
      val betweenness = node.getAttribute(betweennessCentralityColumn).asInstanceOf[Double]
      val closeness = node.getAttribute(closenessCentralityColumn).asInstanceOf[Double]
      val eigenvector = node.getAttribute(eigenvectorCentralityColumn).asInstanceOf[Double]
      val degree = node.getAttribute(degreeColumn).asInstanceOf[Int]
      val community = node.getAttribute(modularityClassColumn).asInstanceOf[Int]
      Friend(
        id, name, picture,
        closeness=closeness, betweenness=betweenness, eigenvector=eigenvector, degree=degree,
        community=community)
    }

    def toJSON = toFriend.toJSON
  }

  private def friendToNode(friend: Friend): Node = {
    val node = graphModel.factory().newNode(friend.fbId)
    node.setAttribute(nameColumn, friend.name)
    node.setAttribute(pictureColumn, friend.picture)
    node.setAttribute(betweennessCentralityColumn, Double.box(friend.betweenness))
    node.setAttribute(closenessCentralityColumn, Double.box(friend.closeness))
    node.setAttribute(eigenvectorCentralityColumn, Double.box(friend.eigenvector))
    node.setAttribute(degreeColumn, Int.box(friend.degree))
    node.setAttribute(modularityClassColumn, Int.box(friend.community))
    node
  }

  private implicit class FriendshipEdge(val edge: Edge) {
    def toFriendship = Friendship(edge.getSource.getId.asInstanceOf[String], edge.getTarget.getId.asInstanceOf[String])
    def toJSON = toFriendship.toJSON
  }

  def toJSON: JsValue = JsObject(Seq(
    "nodes" -> JsArray(graph.getNodes.toCollection.asScala.map(_.toJSON).toSeq),
    "edges" -> JsArray(graph.getEdges.toCollection.asScala.map(_.toJSON).toSeq)
  ))

}