package com.frobinet.uclsocialnetwork

import java.io.FileNotFoundException

import grizzled.slf4j.Logging
import org.scalatra.{ActionResult, BadRequest, Ok, Unauthorized}
import play.api.libs.json._
import play.api.libs.ws.{WSRequest, WSResponse}
import play.api.libs.ws.ning.NingWSClient

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.io.Source

object FacebookAPI extends Logging {

  private final val appToken =
    try { readFirstLine("app-token") }
    catch { case e: Exception => error(s"Couldn't read app-token"); e.printStackTrace() }

  private def readFirstLine(filename: String): String = {
    val file = new java.io.File(s"./secret/$filename")
    debug(s"Trying to read ${file.getCanonicalFile}")
    val src = Source.fromFile(file)
    val line = src.getLines.next()
    src.close
    line
  }

  private final val neededPermissions = Set("user_friends", "public_profile")

  private val Http = NingWSClient()

  // URLs
  private val prefix = "https://graph.facebook.com/v2.8/"
  private def debugTokenURL(token: String) = prefix + s"debug_token?input_token=$token&access_token=$appToken"
  private val meURL = prefix + "me"

  private def request(url: String, token: String, params: (String, String)*): WSRequest =
    Http.url(url).withQueryString(params :+ "access_token" -> token:_*)

  private def get(url: String, token: String, params: (String, String)*): Future[WSResponse] = {
    val req = request(url, token, params:_*)
    debug(s"Facebook Request: GET ${req.url}")
    req.withHeaders("Accept" -> "application/json").withFollowRedirects(false).get()
  }

  private def success[T](ifUnsuccessful: => T, action: => T)(implicit response: WSResponse): T = {
    if(!(200 until 400).contains(response.status)) {
      error(s"Failure: $response")
      ifUnsuccessful
    } else {
      info(s"Success: $response")
      info(s"Response Body: ${response.body}")
      action
    }
  }

  // Public methods
  def fetchFields(token: String, fields: Set[FacebookField]): Future[Map[String, JsLookupResult]] =
    get(meURL, token, "fields" -> fields.map(_.value).mkString(",")).flatMap { implicit response =>
      success(FacebookException(response.body).toFuture, {
        val json = Json.parse(response.body)
        val fieldsMap = fields.map(f => f.value -> json \ f.value).toMap
        Future.successful(fieldsMap)
      })
    }

  def fetchPicture(token: String): Future[String] =
    get(meURL + "/picture", token, "type" -> "large", "redirect" -> "false").flatMap { implicit response =>
      success(FacebookException(response.body).toFuture, {
        warn(s"Response body: ${response.body}")
        val json = Json.parse(response.body)
        val url = (json \ "data" \ "url").get.asInstanceOf[JsString].value
        Future.successful(url)
      })
    }

  def fetchFriends(token: String): Future[Set[String]] =
    get(meURL + "/friends", token).flatMap { implicit response =>
      success(FacebookException(response.body).toFuture, {
        val json = Json.parse(response.body)
        val friendsSeq = (json \ "data").get.asInstanceOf[JsArray].value
        val friends = friendsSeq.map(_ \ "id").map(_.get.asInstanceOf[JsString].value).toSet
        debug(s"Successfully grabbed the friends: $friends")
        Future.successful(friends)
      })
    }

  def checkTokenValidity(token: String, claimedId: String, adminIds: Set[String]): Future[ActionResult] =
    Http.url(debugTokenURL(token)).get().map(implicit response => success(BadRequest("Couldn't debug token via Facebook's API"), {
      val json = Json.parse(response.body)
      val validity = (json \\ "is_valid").head.asInstanceOf[JsBoolean].value
      if(!validity) Unauthorized("Token is invalid")
      else {
        val userId = (json \\ "user_id").head.asInstanceOf[JsString].value
        if (userId != claimedId && !adminIds.contains(userId)) BadRequest("Token id doesn't correspond to request id")
        else {
          // Either id matches the claimed id, or id is an admin
          val scopes = (json \\ "scopes").head.asInstanceOf[JsArray].value.map(_.asInstanceOf[JsString].value).toSet
          if (!neededPermissions.subsetOf(scopes))
            BadRequest(s"Token doesn't allow all the following permissions: ${neededPermissions.mkString(",")}")
          else Ok()
        }
      }
    }))


  case class FacebookException(message: String) extends Exception {
    def toFuture = Future.failed(this)
  }


  sealed abstract class FacebookField(val value: String)
  object fields {
    object Name extends FacebookField("name")
    object Id extends FacebookField("id")
  }

}
