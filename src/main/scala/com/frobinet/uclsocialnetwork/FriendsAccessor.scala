package com.frobinet.uclsocialnetwork

import java.sql.Timestamp

import slick.driver.PostgresDriver.api._

import scala.concurrent.{ExecutionContext, Future}
import FriendsAccessor._

class FriendsAccessor(db: Database)(implicit val executor: ExecutionContext) {

  def getAllFriends: Future[List[Friend]] = db.run(Friends.to[List].result)

  def getAllFriendships: Future[List[Friendship]] = db.run(Friendships.to[List].result)

  def getFriendsFrom(id: String): Future[List[Friend]] = {
    val query = for {
      friendship <- Friendships if friendship.aId === id || friendship.bId === id
      friend <- Friends if (friend.id === friendship.aId || friend.id === friendship.bId) && friend.id =!= id
    } yield friend
    db.run(query.to[List].result)
  }

  def getAllFriendsAndFriendships = Friends.to[Set].result zip Friendships.to[Set].result

  def createFriend(id: String, name: String, picture: String, friendships: Set[Friendship]): Future[Unit] = {
    val friend = Friend(id, name, picture)
    db.run(DBIO.seq(Friends += friend, Friendships ++= friendships).transactionally).map(_ => {
      SocialNetwork.addFriends(Set(friend))
      SocialNetwork.addFriendships(friendships)
    })
  }

  def createFriendship(friendship: Friendship): Future[Unit] = createFriendships(Set(friendship))

  def createFriendships(friendships: Set[Friendship]): Future[Unit] =
    db.run(Friendships ++= friendships) map { _ => SocialNetwork.addFriendships(friendships) }

  def deleteFriend(id: String): Future[Unit] =
    db.run(Friends.filter(_.id === id).delete).map { _ => SocialNetwork.removeFriend(id) }

  def deleteFriendship(idA: String, idB: String): Future[Unit] =
    db.run(Friendships.filter(f => f.aId === idA && f.bId === idB).delete).map { _ =>
      SocialNetwork.removeFriendship(idA, idB)
    }

}

object FriendsAccessor {


  val Friends = TableQuery[FriendTable]
  class FriendTable(tag: Tag) extends Table[Friend](tag, "FRIEND") {
    def id = column[String]("FRIEND_ID", O.PrimaryKey)
    def name = column[String]("FRIEND_NAME") // TODO Limit name length
    def picture = column[String]("FRIEND_PICTURE")
    def community = column[Int]("FRIEND_COMMUNITY")
    def friends = Friendships.filter(f => f.aId === id || f.bId === id)
    def subscriptionTime = column[Timestamp]("SUBSCRIPTION_TIMESTAMP", O.Default(new Timestamp(System.currentTimeMillis())))
    def * = (id, name, picture, subscriptionTime) <> ((Friend.applyDB _).tupled, Friend.unapplyDB)
  }


  val Friendships = TableQuery[FriendshipTable]
  class FriendshipTable(tag: Tag) extends Table[Friendship](tag, "FRIENDSHIP") {
    def aId = column[String]("A")
    def bId = column[String]("B")
    def creationTimestamp = column[Timestamp]("CREATION_TIMESTAMP", O.Default(new Timestamp(System.currentTimeMillis())))
    def * = (aId, bId, creationTimestamp) <> ((Friendship.apply _).tupled, Friendship.unapply)

    def aFK = foreignKey("A_FK", aId, Friends)(_.id, onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.Cascade)
    def bFK = foreignKey("B_FK", bId, Friends)(_.id, onUpdate=ForeignKeyAction.Cascade, onDelete=ForeignKeyAction.Cascade)
    def pk = primaryKey("A_B", (aId, bId))
  }

}
