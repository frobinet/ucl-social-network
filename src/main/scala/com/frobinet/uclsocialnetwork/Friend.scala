package com.frobinet.uclsocialnetwork

import play.api.libs.json._
import java.sql.Timestamp

case class Friend(
  fbId: String, name: String, picture: String,
  degree: Int = 0, eigenvector: Double = 0, closeness: Double = 0, betweenness: Double = 0,
  community: Int = 0,
  subscriptionTime: Timestamp = new Timestamp(System.currentTimeMillis())
) {

  def toJSON = JsObject(Seq(
    "id" -> JsString(fbId),
    "name" -> JsString(name),
    "picture" -> JsString(picture),
    "eigenvector" -> JsNumber(eigenvector),
    "degree" -> JsNumber(degree),
    "closeness" -> JsNumber(closeness),
    "betweenness" -> JsNumber(betweenness),
    "community" -> JsNumber(community),
    "subscription_time" -> JsNumber(subscriptionTime.getTime)
  ))

  def <->(friend: Friend): Friendship = Friendship(fbId, friend.fbId)
}


object Friend {
  def applyDB(id: String, name: String, picture: String, subscriptionTime: Timestamp): Friend =
    Friend(id, name, picture, subscriptionTime = subscriptionTime)
  def unapplyDB(friend: Friend): Option[(String, String, String, Timestamp)] =
    Some(friend.fbId, friend.name, friend.picture, friend.subscriptionTime)
}