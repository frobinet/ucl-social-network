import com.mchange.v2.c3p0.ComboPooledDataSource
import com.frobinet.uclsocialnetwork.{App, _}
import org.scalatra._
import javax.servlet.ServletContext

import grizzled.slf4j.Logging
import org.eclipse.jetty.servlet.DefaultServlet
import slick.driver.PostgresDriver.api._


/**
 * This is the ScalatraBootstrap bootstrap file. You can use it to mount servlets or
 * filters. It's also a good place to put initialization code which needs to
 * run at application start (e.g. database configurations), and init params.
 */
class ScalatraBootstrap extends LifeCycle with Logging {

  val cpds = new ComboPooledDataSource
  info("Created c3p0 connection pool")

  override def init(context: ServletContext) {
    val db = Database.forDataSource(cpds)

    context.initParameters(org.scalatra.EnvironmentKey) = "production"
    context.mount(new App(context, db), "/*")

  }

  private def closeDbConnection() {
    info("Closing c3po connection pool")
    cpds.close()
  }

  override def destroy(context: ServletContext) {
    super.destroy(context)
    closeDbConnection()
  }
}
