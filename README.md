# UCL Social Network

This application was developed as class project during my studies at UCL. The goal was to 
compare different measures of the "popularity" (centrality) of members of a network, and to study the Louvain Method for community detection.

This repo provides a revised, open-source, version of the code written in Scala. It uses the Facebook API in conjunction with an AngularJS based frontend to display the social network of users registered in the application.

Thanks to Gephi, it also provides a ranking of users according to different [centrality measures](https://en.wikipedia.org/wiki/Centrality). Community detection is also performed using the [Louvain Method](https://en.wikipedia.org/wiki/Louvain_Modularity).
More metrics and statistics could easily be added, and contributions are welcome! 

An online version of the application is available [here](http://51.254.132.47.xip.io/).
If you are part of the UCL community, add yourself to the network to see how you rank!


# Deploying and Running the Application

The application is deployed using `sbt`:

```sh
sbt
> update
> dist
```

Using `dist` will compile the application and make a standalone server available in the `target/dist` folder.
The zip file created by `sbt` in `target/dist` can be shipped to any server.

To launch the standalone server, you must be located in the root of the extracted folder and execute
```sh
./bin/run_server
```

The application runs on port `4500`. If you run it on your local computer, you can access it [here](http://localhost:4500/) as 
long as the server is running. Keep in mind that Facebook API related functionalities won't work on your local machine unless the `localhost` domain 
is specified in the Facebook App control panel, or you edit your `/etc/hosts` file to use a domain that is specified in the control panel as an alias for `127.0.0.1`.


# Configuration and secret files

Files that are located in the `src/main/resources`, in the `secret` and in the `public` folders will be automatically be shipped by sbt.
Only the files located in `public` can be accessed by clients. Sensitive information should always be located in the `secret` folder, and any 
configuration file inside this folder will override the possible configuration from `src/main/resources`.


# Database Server

The server application expect a PostgreSQL server to be running on the same machine. 
For information about the configuration, see `src/main/resources/c3p0.properties`.