#!/bin/sh

# Starts forever, logging stdout and stderr in the server-logs folder, located in the parent folder

forever start -o ../server-logs/stdout.log -e ../server-logs/stderr.log -l ../server-logs/forever-log.log -c bash ./bin/run_server